# -*- coding: utf-8 -*-
# (c) 2012 Bright Interactive Limited. All rights reserved.
# http://www.bright-interactive.com | info@bright-interactive.com

import re

from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
from url_history.models import _get_url_history_queue, _set_url_history_queue


class URLHistoryMiddleware(MiddlewareMixin):
    """
    Stores the most recently visited URLs in the session.

    Requires sessions to be enabled.
    """

    def process_request(self, request):
        track_regex = getattr(settings, 'URL_HISTORY_TRACK_REGEX', False)
        track_ajax = getattr(settings, 'URL_HISTORY_TRACK_AJAX', False)
        track_internals = getattr(settings, 'URL_HISTORY_TRACK_INTERNALS', False)

        if (request.is_ajax() and not track_ajax) or ('jsi18n' in request.get_full_path() and not track_internals):
            return

        if track_regex and not re.search(track_regex, request.path):
            return

        history = _get_url_history_queue(request.session)
        history.append(request.get_full_path())

        _set_url_history_queue(request, history)
